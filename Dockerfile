FROM openjdk:13-jdk

# Install Docker client
ARG DOCKER_VERSION=19.03.8
RUN curl -fsSL https://download.docker.com/linux/static/stable/`uname -m`/docker-$DOCKER_VERSION.tgz | tar --strip-components=1 -xz -C /usr/local/bin docker/docker
